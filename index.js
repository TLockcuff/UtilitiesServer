const express = require('express');
const multer = require('multer');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const serveIndex = require('serve-index')
const path = require('path');
const fs = require('fs');
const app = express();
const port = 8080;

// Timed File Deletion
const timeout = 600000 // 10 minutes
const timers = [];

app.engine('handlebars', exphbs({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

app.use(bodyParser.json());
app.use("/tmp", express.static(__dirname + '/tmp'));
app.use(express.static('views'));
app.use('/tmp', serveIndex(`${__dirname}/tmp`, { 'icons': true, view: 'details' }));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', (req, res) => res.render('home'));
app.get('/pdf', (req, res) => res.render('pdf'));

app.post('/utils/pdf', multer({ dest: './tmp/' }).single('file'), (req, res) => {

    fs.rename(`${__dirname}/tmp/${req.file.filename}`, `${__dirname}/tmp/${req.file.filename}.pdf`);

    const pdfPath = `./tmp/${req.file.filename}.pdf`;
    const imagePath = `./tmp/${req.file.filename}.png`;

    var theOptions = {};
    theOptions.convertOptions = {};
    theOptions.convertOptions["-resize"] = "100%";
    theOptions.convertOptions["-density"] = "400";

    var PDFImage = require("pdf-image").PDFImage;
    var pdfImage = new PDFImage(pdfPath, theOptions);

    pdfImage.convertPage(0).then(function (imagePath) {
        var origin = req.get('origin');
        res.send({ image: `${origin}/tmp/${req.file.filename}-0.png` })
        fs.unlink(`${__dirname}/tmp/${req.file.filename}.pdf`);

        const timer = setTimeout(() => fs.unlink(`${__dirname}/tmp/${req.file.filename}-0.png`), timeout);
        timers.push(timer);

    }, function (err) {
        fs.unlink(`${__dirname}/tmp/${req.file.filename}.pdf`);
        res.send(err, 500);
    });

});

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
